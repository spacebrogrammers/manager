namespace SpaceBrogrammers.Manager
{
	using UnityEngine;

	public class Manager<T> : MonoBehaviour where T : Manager<T>
	{
		protected static T singleton;
		public static T Singleton
		{
			get { return singleton; }
			protected set { singleton = value; }
		}

		protected virtual void Awake()
		{
			Singleton = this as T;
		}
	}
}
