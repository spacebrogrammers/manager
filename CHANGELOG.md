# Changelog
All notable changes to this package are documented in this file, starting from the release commit.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

<br>

## [[v1.0.1]] - 2022.05.26
[v1.0.1]: https://gitlab.com/spacebrogrammers/Manager/-/compare/1.0.0...1.0.1

* Added README
* Added LICENSE
* Fixed manager double instantiation

<br>

## [[v1.0.0]] - 2022.05.03
[v1.0.0]: https://gitlab.com/spacebrogrammers/Manager/-/tags/1.0.0

* First release





