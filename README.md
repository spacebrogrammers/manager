# Manager
> Base Singleton class that autoinstantiate

![Add Badges](https://img.shields.io/badge/version-v1.0.1-orange)

## Installation

Open the file Packages/manifest.json and insert under the packages the line:
"com.spacebrogrammers.unity.manager": "ssh://git@gitlab.com/spacebrogrammers/manager.git",

## Includes

* **Manager**: abstract MonoBehaviour with Singleton static reference.
* **SmartManager**: abstract MonoBehaviour based on *Manager* with features such as automatic on call instantiation and initialization.
* **ShutDownObserver**: utility class with public static parameter to know when the application is shutting down.

## Usage example

Create your own manager making it inherit from Manager or SmartManager. 
From another class you can access your manager through the singleton variable.

```C#
using SpaceBrogrammers.Manager;
public class User
{
    public void CallManager()
    {
        GameManager.singleton.Foo();
    }
}

public class GameManager : SmartManager<GameManager>
{
    public void Foo()
    {
        
    }
}
```

## Meta

spacebrogrammers@gmail.com 

[spacebrogrammers gitlab](https://gitlab.com/spacebrogrammers)


## Contributing

1. Fork it 
2. Create your feature branch (git checkout -b feature/fooBar)
3. Commit your changes (git commit -am 'Add some fooBar')
4. Push to the branch (git push origin feature/fooBar)
5. Create a new Pull Request
