﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SpaceBrogrammers.Manager
{
	public class ShutdownObserver
	{
		public static bool IsShuttingDown { get; private set; }


		static void Quit()
		{
			IsShuttingDown = true;
		}

		[RuntimeInitializeOnLoadMethod]
		static void ObserversDelegates()
		{
			Application.quitting -= Quit;
			Application.quitting += Quit;
		}
	}
}
