﻿namespace SpaceBrogrammers.Manager
{
	using UnityEngine;

	public abstract class SmartManager<T> : Manager<T> where T : SmartManager<T>
	{
		public new static T Singleton
		{
			get { return InstantiatedSingleton(); }
			protected set { singleton = value; }
		}

		public static T InstantiatedSingleton()
		{
			if(singleton == null
#if UNITY_EDITOR
					&& UnityEditor.EditorApplication.isPlaying
#endif
					&& Application.isPlaying && !ShutdownObserver.IsShuttingDown && !unloading)
				singleton = new GameObject(typeof(T).ToString()).AddComponent<T>();
			return singleton;
		}

		static bool unloading;

		public static string DebugTitle { get { return "[" + typeof(T).ToString().ToUpperInvariant() + "]"; } }

		protected virtual bool InitOnAwakeOverride { get { return initOnAwake; } }

		[Header("Manager")]
		[SerializeField] bool initOnAwake = true;

		protected override void Awake()
		{
			if(singleton != null)
			{
				Debug.LogError(DebugTitle + " ERROR: Double singleton.");
				gameObject.SetActive(false);
			}
			else
			{
				gameObject.name = typeof(T).ToString();
				singleton = (T)this;
				if(InitOnAwakeOverride)
					Init();
			}
		}

		protected virtual void OnDestroy()
		{
			unloading = true;
			if(singleton == this) Destroyed();

			UnityEngine.SceneManagement.SceneManager.sceneUnloaded -= UnloadingDone;
			UnityEngine.SceneManagement.SceneManager.sceneUnloaded += UnloadingDone;
		}

		static void UnloadingDone(UnityEngine.SceneManagement.Scene s)
		{
			UnityEngine.SceneManagement.SceneManager.sceneUnloaded -= UnloadingDone;
			unloading = false;
		}

		public virtual void Init(System.Action<bool> callback) { Init(); callback?.Invoke(true); }
		protected virtual void Init() { }
		protected virtual void Destroyed() { }
	}
}
